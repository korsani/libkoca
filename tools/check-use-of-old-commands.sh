#!/usr/bin/env bash
if [ -z "$1" ] ; then
	echo "$0 <file> [ <file> [ ... ] ] "
fi
oldc=( $(bash /usr/local/include/libkoca.sh list | awk '{print $1}' | sed -e 's/koca_//') )
while  [ -n "$1" ] ; do
	file="$1"
	echo ">>> $file"
	for c in ${oldc[@]} ; do
		grep -n --color -E "(^| |\()$c" "$1"
	done
	shift
done
	
