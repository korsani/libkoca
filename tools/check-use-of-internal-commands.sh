#!/usr/bin/env bash
SRC_DIR="$1"
function get_funcs() {
	grep -E "^function" "$SRC_DIR/"*.sh | awk '{print $2}'
}
#echo "Checking wether files in $SRC_DIR still reference old func name"
mapfile -t funcs < <(get_funcs)
_ec=0
for f in "${funcs[@]}" ; do
	grep -HnE "^\s*[^#].*\s+:x${f/koca_/} " "$SRC_DIR/"*.sh && _ec=1
	grep -HnE "^\s*${f/koca_/} " "$SRC_DIR/"*.sh && _ec=1
done
exit $_ec
