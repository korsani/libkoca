#!/usr/bin/env bash
d=$( git log --pretty=%ct -1 | sed -e 1b -e '$!d' | xargs | tr ' ' '-' | bc)		# Nombre de secondes depuis le 1er commit
# La version sera mois.jours.heurs.minutes depuis le 1er commit
printf '%d.%d.%d.%d\n' $(bc <<< "month=60*60*24*30;day=60*60*24;hour=60*60;min=60;$d/month;($d%month)/day;($d%month%day)/hour;($d%month%day%hour)/min")
