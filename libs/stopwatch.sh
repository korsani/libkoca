#!/usr/bin/env bash
function koca_stopwatch() {	# Provide a stopwatch, ms precision
	function _del() {
		local s="$1"
		"$KOCA_GNU_SED" -i"" -e "/^$s=/d" "$timer_file"	# Remove timer from file
	}
	function _get() {
		if ! [ -s "$timer_file" ] || ! grep -qE "^$name=" "$timer_file" ; then		# If file does not exists or there is no timer in it
			return 2
		fi
		local s ; s="$(awk -F "=" "/^$name=/{t=\$2}END{print t}" "$timer_file")"			# Take last timer of that name
		LC_ALL=C printf "%f\n" "$( bc -l <<<"( $($KOCA_GNU_DATE +%s%N) - $s ) / 1000000000" )" 		# Give ms
	}
	local opt name; opt="$1" ; shift ; names=( "$@" )
	if [ "${#names[@]}" -eq 0 ] ; then
		names=("default")
	fi
	local timer_file="/tmp/.koca_stopwatch_timers.$$.$(id -u)"
	local date
	for name in "${names[@]}" ; do
		if [[ $name =~ .*=.* ]] ; then
			echo "[__libname__] Timer's name '$name' contains forbidden chars " >&2
			return 3
		fi
	done
	case "$opt" in
		start)
			for name in "${names[@]}" ; do
				printf '%s=%i\n' "$name" "$($KOCA_GNU_DATE +%s%N)" >> "$timer_file"
			done
			return
			;;
		reset)
			for name in "${names[@]}" ; do
				_del "$name"
				printf '%s=%i\n' "$name" "$($KOCA_GNU_DATE +%s%N)" >> "$timer_file"
			done
			return
			;;
		delete|rm)
			for name in "${names[@]}" ; do
				_del "$name"
			done
			return
			;;
		stop)
			for name in "${names[@]}" ; do
				_get "$name" || return 2
				_del "$name"
			done
			;;
		get)
			for name in "${names[@]}" ; do
				_get "$name" || return 2
			done
			;;
		list|ls)
			if ! [ -s "$timer_file" ] ; then		# If file does not exists or there is no timer in it
				return 2
			else
				cut -d "=" -f 1 "$timer_file" | sort -u
			fi
			;;
		*)
			echo "Duno how to do '$opt'"; return 4;;
	esac
	if ! [ -s "$timer_file" ] ; then		# Remove empty file
		rm -f "$timer_file"
	fi
}
