#!/usr/bin/env bash
function koca_dieIfNotRoot() { # Exit calling script if not running under root. Usage: $0
	local src ; src=__libkoca__ ; [ -e $src ] && eval "$(bash $src gotRoot)"
	! koca_gotRoot && echo "[__libname__] Actually, I should be run as root" && exit 1
	#! underSudo && echo "[__libname__] Actually, I should be run under sudo" && exit 1
	return 0
}
function koca_dieIfRoot() { # Exit calling script if run under root
	local src ; src=__libkoca__ ; [ -e $src ] && eval "$(bash $src gotRoot)"
	koca_gotRoot && echo "[__libname__] I should not be run as root" && exit 1
	#underSudo && echo "[__libname__] I should not be run under sudo" && exit 1
	return 0
}
function koca_underSudo() { # Return wether the calling script is run under sudo
	[ -n "$SUDO_USER" ]
}
function koca_gotRoot() { # Return wether the calling script is run under root
	[ "$(id -u)" -eq 0 ]
}
