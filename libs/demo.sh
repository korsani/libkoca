#!/usr/bin/env bash
function koca_demo {
	tput civis
	echo ">>> koca_spin"
	for s in $(koca_spin list | cut -d ':' -f 1 | tac) ; do
		echo "Spin $s"
		for i in {1..20} ; do
			koca_spin $s
			sleep 0.1
		done
		echo
	done
	for i in {1..20} ; do
		koca_spin azertyazertyazertyazerty 0 10
		sleep 0.2
	done
	echo
	echo ">>> koca_progress"
	export COLUMNS=$(tput cols)
	for i in {1..100} ; do
		koca_progress $i " roughly $i%" 5
		sleep 0.2
	done
}
