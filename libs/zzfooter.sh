#!/usr/bin/env bash
_outdated lockMe
_outdated cleanOnExit
_outdated isIp
_outdated dhms2s
_outdated checkNeededFiles
_outdated s2dhms
_outdated dieIfNotRoot
_outdated gotRoot
_outdated dieIfRoot
_outdated dieIfNotRoot
_outdated whereAmI
_outdated whereIs
_outdated getColor
_outdated getConfValue
_outdated getConfAllKeys
_outdated getConfAllSections

HAS_LIBKOCA='y'
# Compatibility
KOCA_IMPORTED=1

eval "$(_koca_compat_vars)"
# Parenthese guarantee that my variables won't pollute the calling shell
(
me="$(basename -- "$0")"
# __libname__ will be replaced by the filename
KOCA_LIBNAME='__libname__'
KOCA_VERSION='__version__'
# exit if I'am sourced from a shell
if [ "$me" != "$KOCA_LIBNAME" ] ; then
	exit 0
fi
here="$(cd "$(dirname "$0")" ; pwd)"
# full path to me
fp2me="${here}/$me"
if [ $# -eq 0 ] ; then
	echo
	echo "	$me - usefull shell functions"
	echo
	echo "# Import all the functions"
	echo "$ source $me"
	echo "# List all the functions that can be imported"
	echo "$ $me --list"
	echo "#Import only some functions"
	echo "$ eval \"\$(bash $me function [ function [ ... ] ])\""
	echo "# Don't forget \" around !"
	echo "# Show examples of few functions"
	echo "$ $me --demo"
	echo "# Show version"
	echo "$ $me --version"
	exit
fi
while [ -n "$1" ] ; do
	case "$1" in
		--list|-l)		"$KOCA_GNU_GREP" -E '^function' "$0" | sed -e 's/function *//' -e 's/{\(\)//g' -e 's/()//' ; exit ;;
		--demo|-d)		koca_demo;;
		--version|-v)	echo "$KOCA_VERSION";;
		*)
			if ! "$KOCA_GNU_GREP" -qE "^function $1" "$0" ; then
				echo "[$libname] function $1 does not exist inside me"
				exit 1
			fi
			if [ "$(type -t "$1")" == "function" ] ; then
				type -a "$1" | sed -e "s#__libkoca__#$fp2me#g" | tail -n +2
				_koca_compat_vars
			fi
			;;
	esac
	# Print code of the function
	# plus linking
	shift
done
)
