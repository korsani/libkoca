#!/usr/bin/env bash
function koca_cpusage() { # Compute elapsed time, user time and system time consumed by the process
	case $(uname) in
		Linux)
			# Thanks to https://www.baeldung.com/linux/total-process-cpu-usage
			local tick="$(getconf CLK_TCK)" a u elapsed utime stime
			read -r u a </proc/uptime ; u="${u/./}"					# uptime, in ms
			mapfile -t -d ' ' stats <"/proc/$$/stat"
			elapsed="$(bc -l<<<"$u/100-${stats[21]}/$tick")"		# uptime - starttime, in s
			utime="$(bc -l<<<"(${stats[13]}+${stats[15]})/$tick")"
			stime="$(bc -l<<<"(${stats[14]}+${stats[16]})/$tick")"
			;;
		FreeBSD)
			IFS=" " read -r utime stime < <(LC_ALL=C ps -p $$ -So usertime= -o systime= )	# Precision is seconds
			utime="$(sed -e 's/:/*60*60+/' -e 's/\./*60+/' <<<"$utime"|bc)"
			stime="$(sed -e 's/:/*60*60+/' -e 's/\./*60+/' <<<"$stime"|bc)"
			lstart="$( LC_ALL=C ps -p $$ -So lstart=)"		# same
			lstart="$( LC_ALL=C "$KOCA_GNU_DATE" -j -f "%a %b %d %T %Z %Y" "$lstart" "+%s")"
			elapsed="$(( EPOCHSECONDS - lstart ))"
			;;
		OpenBSD)
			echo -e "\nOpenBSD: To be done\n" >&2
			echo 0 0 0
			return 1
			;;
	esac
	LC_ALL=C printf "%.03f %.03f %.03f" "$elapsed" "$utime" "$stime"
	# Add \n if stdout is a terminal
	[ -t 1 ] && echo
}
