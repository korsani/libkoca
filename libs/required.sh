#!/usr/bin/env bash
function koca_required() {
	if [ -n "${1:-}" ] && [[ ${1:-} =~ [0-9.] ]]  ; then
		# If asked version is lower than lib version (i.e.: is lib version is lower than asked version),
		# wipe every function defined here
		if [ "__version__" == "$(echo -e "$1\n__version__" | sort -V | head -1)" ] && [ "$1" != "__version__" ] ; then
			unset $(set | grep -E '^koca_.* ()$' | cut -d ' ' -f 1)
			printf '[__libname__] Asked version %s is above lib version %s. Func were unimported.\n' "$1" "__version__" >&2
			unset HAS_LIBKOCA
		fi
	fi
}
