#!/usr/bin/env bash
function koca_urlEncode() {	# Url-encode a string
	local in
	if [ -z "${1:-}" ] ; then
		if ! [ -t 0 ] ; then
			in="$(cat)"
		else
			return 1
		fi
	else
		in="$1"
	fi
	local reserved
	# é è ê à â ô ù û ç
	reserved=( '%' ' ' "æ" "Æ" "œ" "Œ" "ò" "à" "â" "ä" "é" "è" "ê" "ë" "î" "ï" "ô" "ö" "ù" "û" "ü" "ÿ" "ç" "À" "Â" "Ä" "Ç" "É" "È" "Ê" "Ë" "Î" "Ï" "Ô" "Ö" "Ù" "Û" "Ü" "Ÿ" ':' '/' '?' '#' ']' '@' '!' '&' "'" '(' ')' '*' '+' ',' ';' '=' )
	hex=( "%25" "%20" "%C3%A6" "%C3%86" "%C5%93" "%C5%92" "%C3%B2" "%C3%A0" "%C3%A2" "%C3%A4" "%C3%A9" "%C3%A8" "%C3%AA" "%C3%AB" "%C3%AE" "%C3%AF" "%C3%B4" "%C3%B6" "%C3%B9" "%C3%BB" "%C3%BC" "%C3%BF" "%C3%A7" "%C3%80" "%C3%82" "%C3%84" "%C3%87" "%C3%89" "%C3%88" "%C3%8A" "%C3%8B" "%C3%8E" "%C3%8F" "%C3%94" "%C3%96" "%C3%99" "%C3%9B" "%C3%9C" "%C5%B8"  "%3A" "%2F" "%3F" "%23" "%5D" "%40" "%21" "%26" "%27" "%28" "%29" "%2A" "%2B" "%2C" "%3B" "%3D" )
	for char in "${!reserved[@]}" ; do
		#printf -v e "%%%x" "'${reserved[$char]}"
		sed_args+=" -e \"sx${reserved[$char]}x${hex[$char]}xg\""
	done
	in="$(eval sed $sed_args <<< "$in")"
	# Special cases
	in="$(sed -e "sx\[x%5Bxg" <<< "$in")"
	in="$(sed -e "sx\\\$x%24xg" <<< "$in")"
	echo "$in"
}
