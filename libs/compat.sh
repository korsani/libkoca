#!/usr/bin/env bash
function _koca_find_date() {
	local d
	for d in date gdate ; do
		if $d --version 2>/dev/null | grep -q 'Free Software' ; then		# Simple
			which $d
			return
		fi
	done
	echo "[__libname__] No GNU date(1) found. Expect troubles" >&2
	return 1
}
function _koca_find_sed() {
	local s
	for s in sed gsed ; do
		if $s --version 2>/dev/null | grep -q 'Free Software' ; then		# Simple
			which $s
			return
		fi
	done
	echo "[__libname__] No GNU sed(1) found. Expect troubles" >&2
	return 1
}
function _koca_find_wc() {
	local w
	for w in wc gwc ; do
		if $w --version 2>/dev/null | grep -q 'Free Software' ; then		# Simple
			which $w
			return
		fi
	done
	echo "[__libname__] No GNU wc(1) found. Expect troubles" >&2
	return 1
}
function _koca_find_bc() {
	local b
	for b in bc bc-gh ; do
		if $b --version 2>/dev/null | grep -iq "free software" ; then
			which $b
			return
		fi
	done
	echo "[__libname__] No GNU bc(1) found. Expect troubles" >&2
}
function _koca_find_grep() {
	local g
	for g in grep ggrep ; do
		if $g --version 2>/dev/null | grep -iq "free software" ; then
			which $g
			return
		fi
	done
	echo "[__libname__] No GNU grep(1) found. Expect troubles" >&2
}
function _koca_compat_vars() {
	echo KOCA_GNU_DATE=\"$(_koca_find_date)\"
	echo KOCA_GNU_BC=\"$(_koca_find_bc)\"
	echo KOCA_GNU_WC=\"$(_koca_find_wc)\"
	echo KOCA_GNU_SED=\"$(_koca_find_sed)\"
	echo KOCA_GNU_GREP=\"$(_koca_find_grep)\"
}
