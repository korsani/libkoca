#!/usr/bin/env bash
function koca_timer() {		# Timer, countdown, wait
	local i ivis reverse timer start end
	while [ -n "$1" ] ; do
		case $1 in
			-i)		ivis="y";;
			-r)		reverse="y";;
			[0-9]*)	timer="$1";;
		esac
		shift
	done
	if [ -z "$timer" ] ; then
		echo "[__libname__] Dunno where to start (or end)" >&2
		return 1
	fi
	if [ -n "$ivis" ] ; then
		tput civis
	fi
	if [ -n "$reverse" ] ; then
		start="$timer"
		end=1
		d=-1
	else
		start=1
		end=$timer
		d=1
	fi
	for i in $(seq "$start" "$d" "$end") ; do
		echo -ne "\r$i "
		sleep 1
	done
	echo -ne "\r$((i-1))"
	if [ -n "$ivis" ] ; then
		tput cnorm
	fi
}
