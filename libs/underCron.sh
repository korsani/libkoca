#!/usr/bin/bash
function koca_underCron() {	# Return true if script is run by cron-like
	local pid="${1:-$$}" cmd ps_opt_command
	case $(uname) in
		Darwin|FreeBSD)	ps_opt_command="command=";;
		*)				ps_opt_command="cmd=";;
	esac
	while [ $pid -ne 0 ] ; do
		cmd="$(basename -- "$(ps -o $ps_opt_command "$pid" | cut -d ' ' -f 1)")"
		# Tested only under fcron and vixie-cron
		# "\ ?.*" to match "cron -f"
		if [[ "$cmd" =~ ^cron$ ]] || \
			[[ "$cmd" =~ ^fcron$ ]] || \
			[[ "$cmd" =~ ^bcron-exec$ ]] || \
			[[ "$cmd" =~ ^anacron$ ]] || \
			[[ "$cmd" =~ ^crond$ ]] || \
			[[ "$cmd" =~ ^mcron$ ]] ; then
					return 0
		fi
		pid="$(ps -o ppid= "$pid" | tr -d ' ')"
	done
	return 1
}
