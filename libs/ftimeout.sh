#!/usr/bin/env bash
function koca_ftimeout() {	# kill a function after certain time. koca_ftimeout n function [ function_parameter ... ]
	local t="$1" f="$2" s ccpid cpid ; shift 2
	# Thanks to https://superuser.com/questions/305933/preventing-bash-from-displaying-done-when-a-background-command-finishes-execut
	{ 2>&3 $f $@ & } 3>&2 2>&-
	disown &>/dev/null
	# children pid
	cpid="$!"
	# $! is the launching bash, I want children's children pid
	# On Linux I could use ps -o pid --ppid $!
	# But that's not compatbile with Darwin
	ccpid="$(ps -o ppid,pid | awk "{if(\$1==$cpid) {print \$2} }")"
	#ccpid="$(ps -o ppid,pid | awk "/^ +$$ /{print \$2}" | head -1)"
	s="$SECONDS"
	while [ $((SECONDS-s)) -lt "$t" ] ; do
		if ! ps "$ccpid" >/dev/null 2>&1 ; then
			# son returns before curfew
			return
		else
			sleep 0.5
		fi
	done
	# son is late
	# killing children and children's child prevent "Terminated" message
	kill "$ccpid" "$cpid" 2>/dev/null
}
