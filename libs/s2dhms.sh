#!/usr/bin/env bash
function koca_s2dhms() {	# Convert seconds to day hour min sec, or xx:xx:xx if -I. Usage: $0 [ -r | -I ] [ -x ] <int>
	export PS4=" \$LINENO "
	local FORMAT=0 w="" ROUND="" n="" EXT="0"
	while [ -n "${1:-}" ] ; do
		case $1 in
			-I)	FORMAT=1;;
			-r)	ROUND=1;;
			-x) EXT=1;;
			*)	w="$1";;
		esac
		shift
	done
	if [ -z "$w" ] ; then
		read -r w
	fi
	if ! [[ $w =~ ^[0-9]+$ ]] ; then
		echo '    NaN    '
		return
	fi
	#if [ "$FORMAT" -eq 1 ] && [ "$w" -ge 8640000 ] ; then
	#	echo '    OoR    '
	#	return
	#fi
	if [ -n "$ROUND" ] ; then
		# Haha! tricky but nice algorithm!
		# List of durations
		declare -A seconds; seconds=( [kalpa]=136235520000000000 [eon]=31536000000000000 [century]=3153600000 [year]=31536000 [week]=604800 [day]=86400 [hour]=3600 [min]=60 [sec]=1)
		# List of units
		declare -A units; units=( [kalpa]="kalpa" [eon]="eon" [century]="c" [year]="y" [week]="w" [day]="d" [hour]="h" [min]="min" [sec]="s" )
		# List of duration I'll use
		declare -a order; order=( year week day hour min sec )
		if [ $EXT -eq 1 ] ; then
			# I use those one also
			order=( kalpa eon century ${order[@]} )
			local kalpa="$((w/seconds[kalpa]))"; 	w=$((w%seconds[kalpa]))
			local eon="$((w/seconds[eon]))"; 		w=$((w%seconds[eon]))
			local century="$((w/seconds[century]))";w=$((w%seconds[century]))
		fi
		local year="$((w/seconds[year]))";		w=$((w%seconds[year]))
		local week="$(( w/seconds[week] ))";	w=$((w%seconds[week]))
		local day="$(( w/seconds[day] ))";		w=$((w%seconds[day]))
		local hour="$(( w/seconds[hour] ))";	w=$((w%seconds[hour]))
		local min="$(( w/seconds[min]  ))";		sec=$((w%seconds[min]))
		# Loop on (kalpa, eon, century) year, week, days, hours, mins
		for r in $(seq 0 "$(( ${#order[@]} - 2 ))" ) ; do
			local n1="$(eval echo \$${order[$((r+1))]})"
			local n="$(eval echo \$${order[$r]})"
			if [ "$n" -ne "0" ] ; then
				# if next in rank is greater than half
				# Example: if r=year, r+1=week
				if [ "$(( $n1*${seconds[${order[$((r+1))]}]} ))" -gt "$(( ${seconds[${order[$r]}]}/2 ))" ] ; then
					printf "%i%s" "$(( 1 + n ))" "${units[${order[$r]}]}"
				else
					printf "%i%s" "$n" "${units[${order[$r]}]}"
				fi
				return
			fi
		done
	elif [ "$FORMAT" == 1 ] ; then
		bc<<<"
		w=$w
		dw=w/86400;w=w%86400;hw=w/3600;w%=3600;mw=w/60;w%=60;
		if(dw<10) print 0;print dw;\":\";
		if(hw<10) print 0;print hw;\":\";
		if(mw<10) print 0;print mw;\":\";
		if(w<10) print 0;print w;
		"
	else
		bc<<<"
		if ($EXT==1) {
			kw=$w/136235520000000000
			w=$w%136235520000000000
			ew=w/31536000000000000
			w=w%31536000000000000
			cw=w/3153600000
			w=w%3153600000 ;
		}
		if ($EXT==0) {w=$w ;}
		yw=w/31536000;w=w%31536000;ww=w/604800;w=w%604800;dw=w/86400;w=w%86400;hw=w/3600;w%=3600;mw=w/60;w%=60;
		if ($EXT==1) {
			if(kw!=0) print kw,\"kalpa\";
			if(ew!=0) print ew,\"eon\";
			if(cw!=0) print cw,\"c\";
			}
		if(yw!=0) print yw,\"y\";
		if(ww!=0) print ww,\"w\";
		if(dw!=0) print dw,\"d\";
		if(hw!=0) print hw,\"h\";
		if(mw!=0) print mw,\"min\";
		if(w!=0) print w,\"s\";
		"
	fi
}
