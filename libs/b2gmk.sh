#!/usr/bin/env bash
function koca_b2gmk() {	# Round float and suffix it with SI prefix
	export PS4=" \$LINENO " ; export BC_LINE_LENGTH=0
	local w="$1" p="${2:-1024}" sign="1" r n units
	export LC_ALL=C
	if ! [[ "$w" =~ ^[-]?[0-9.]+$ ]] ; then
		echo '?'
		return 1
	fi

	if [ -z "$w" ] ; then
		read -r w
	fi
	if [[ "$w" =~ ^-.* ]] ; then
		sign="-1"
		w=${w#-}
	fi
	if [[ $w =~ ^0*\..* ]] ; then	# if < 1
		n="$w";r=0
		units=('' 'm' 'u' 'n' 'p' 'f' 'a' 'z' 'y' 'r' 'q')
		# Find the p-based power of w
		# int-ing is always a hassle...
		printf -v r "%.0f" "$($KOCA_GNU_BC -l <<<"0.49-l($w)/l($p)")"
		n="$( $KOCA_GNU_BC<<<"scale=64;$w*$p^$r")"
		# Here I have the power of base by which I have to multiply n to be between 0 and base.
		# Which suffix is it?
		if [ "$r" -ge $(( ${#units[@]} - 1)) ] ; then	# Woops
			n="$($KOCA_GNU_BC<<<"scale=64;$w*$p^$(( ${#units[@]} - 1))")"
			koca_b2gmk "$n" "$p"
			r="$(( ${#units[@]} - 1 ))"
			printf "%s" "${units[$r]}"
			return
		fi
		printf "%.3g%s" "$($KOCA_GNU_BC<<<"$sign*$n")" "${units[$r]}"
	elif [ "$w" = "0" ] ; then
		echo -n 0
		return
	else
		units=('' 'k' 'M' 'G' 'T' 'P' 'E' 'Z' 'Y' 'R' 'Q' )
		# int-ing is always a hassle...
		printf -v r "%i" "$($KOCA_GNU_BC -l <<<"l($w)/l($p)"| cut -d '.' -f 1)"
		n=$($KOCA_GNU_BC<<<"scale=2;$w/($p^$r)")
		if [ "$r" -ge ${#units[@]} ] ; then	# Woops
			n="$($KOCA_GNU_BC<<<"scale=2;$w/($p^(${#units[@]}-1))")"
			koca_b2gmk "$n" "$p"
			r="$(( ${#units[@]} - 1 ))"
			printf "%s" "${units[$r]}"
			return
		fi
		printf "%.3g%s" "$n" "${units[$r]}"
	fi
	return
}
