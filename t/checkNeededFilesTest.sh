#!/usr/bin/env bash
# $Id: koca_checkNeededFiles.sh 1132 2012-09-06 15:00:07Z gab $
source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
testCNF() {
	koca_checkNeededFiles may kbash 2>/dev/null; r=$?
	assertEquals "Should return code 1 on 1 failed may" "1" "$r"
	koca_checkNeededFiles must kbash 2>/dev/null; r=$?
	assertEquals "Should return code 1 on 1 failed must" "1" "$r"
	koca_checkNeededFiles must kbash may bash 2>/dev/null; r=$?
	assertEquals "Should return code 1 on 1 failed must" "1" "$r"
	koca_checkNeededFiles must kbash may bash ls 2>/dev/null; r=$?
	assertEquals "Should return code 1 on 1 failed must and 2 ok may" "1" "$r"
	koca_checkNeededFiles may kbash must bash ls may plop 2>/dev/null; r=$?
	assertEquals "Should return code 2 on 2 failed mixed may and 1 ok must" "2" "$r"
	koca_checkNeededFiles may kbash must kbash may plop 2>/dev/null; r=$?
	assertEquals "Should return code 3 on 3 failed mixed may and must" "3" "$r"
	koca_checkNeededFiles may bash ; r=$?
    assertEquals "Should return the path in the variable" "$(which bash)" "$bash"
	koca_checkNeededFiles may dfghj ; r=$?
    assertEquals "Should return the echo in the variable" "echo dfghj" "$dfghj"
	mess=$(koca_checkNeededFiles -q may dfghj) ; r=$?
    assertEquals "Should return the echo in the variable" "" "$mess"

}
source $(cd $(dirname "$0") ; pwd)/footer.sh
