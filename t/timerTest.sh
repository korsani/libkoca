#!/usr/bin/env bash
source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
testTimer() {
	start=$SECONDS ; koca_timer 2 ; end=$SECONDS
	assertEquals '2' "$((end-start))"
	start=$SECONDS ; koca_timer -r 2 ; end=$SECONDS
	assertEquals '2' "$((end-start))"
	start=$SECONDS ; koca_timer -r -i 2 ; end=$SECONDS
	assertEquals '2' "$((end-start))"
}
source $(cd $(dirname "$0") ; pwd)/footer.sh
