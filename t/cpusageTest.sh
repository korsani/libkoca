#!/usr/bin/env bash
source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
testCpUsage() {
	assertEquals "Not returning 3 values" "3" "$(koca_cpusage | $KOCA_GNU_WC -w)"
}
source $(cd $(dirname "$0") ; pwd)/footer.sh
