#!/usr/in/env bash
here="$(cd $(dirname "$0") ; pwd)"
bn="$(basename "$0" Test.sh)"
source "$here/../libs/compat.sh"
KOCA_GNU_DATE="$(_koca_find_date)"
KOCA_GNU_BC="$(_koca_find_bc)"
KOCA_GNU_WC="$(_koca_find_wc)"
KOCA_GNU_SED="$(_koca_find_sed)"
KOCA_GNU_GREP="$(_koca_find_grep)"
if [ -e $here/../libs/$bn.sh ] ; then
	source "$here/../libs/$bn.sh"
else
	echo "Can't include $bn.sh"
	exit 1
fi
