#!/usr/bin/env bash
if [ -n "$(which shunit2)" ] ; then
	source "$(which shunit2)"
fi
if [ -z "$SHUNIT_VERSION" ] ; then
	echo '* shunit2 not found. No tests performed'
	exit 0
fi
