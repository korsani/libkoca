#!/usr/bin/env bash
source "$(cd "$(dirname "$0")" ; pwd)"/bootstrap.sh
testB2Kbase() {
	export BC_LINE_LENGTH=0
	assertEquals "1k" 		"$(koca_b2gmk 		1024)"
	assertEquals "1.99k" 	"$(koca_b2gmk $((	1024*2-1			))	)"
	assertEquals "500k" 	"$(koca_b2gmk $((	500*1024 			))	)"
	assertEquals "1M" 		"$(koca_b2gmk $((	1024**2 			))	)"
	assertEquals "1M"	 	"$(koca_b2gmk $((	1024**2+1 			))	)"
	assertEquals "1.5M" 	"$(koca_b2gmk $((	1024**2+512*1024	))	)"
	assertEquals "500M" 	"$(koca_b2gmk $((	500*1024**2			))	)"
	assertEquals "1G" 		"$(koca_b2gmk $((	1024**3 			))	)"
	assertEquals "500G" 	"$(koca_b2gmk $((	500*1024**3 		))	)"
	assertEquals "1T"	 	"$(koca_b2gmk $((	1024**4 			))	)"
	assertEquals "500T" 	"$(koca_b2gmk $((	500*1024**4 		))	)"
	assertEquals "1P"	 	"$(koca_b2gmk $((	1024**5 			))	)"
	assertEquals "500P"		"$(koca_b2gmk $((	500*1024**5 		))	)"
	assertEquals "1P"		"$(koca_b2gmk $((	1024**5+1024**3 	))	)"
	assertEquals "100"		"$(koca_b2gmk 		100						)"
	assertEquals "300"		"$(koca_b2gmk 		300						)"
	assertEquals "1k"		"$(koca_b2gmk 		1025					)"
	assertEquals "0" 		"$(koca_b2gmk 		0						)"
	assertEquals "3.94R" 	"$(koca_b2gmk "$($KOCA_GNU_BC <<<'3.94*1024^9')"			)"
	assertEquals "3.94MQ" 	"$(koca_b2gmk "$($KOCA_GNU_BC <<<'3940.8*10^33')" 1000	)"
	assertEquals "395kQ" 	"$(koca_b2gmk "$($KOCA_GNU_BC <<<'394.8*10^33')" 1000	)"
	assertEquals "10GQQ" 	"$(koca_b2gmk "$($KOCA_GNU_BC <<<'10^70')" 1000		)"
}
testB2Kbase2() {
	assertEquals "1m" 		"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-1"				)")"
	assertEquals "2m"	 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	2*1024^-1 - 1024^-2 "	)")"
	assertEquals "500m" 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	500*1024^-1"			)")"
	assertEquals "1u"	 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-2"				)")"
	assertEquals "1u"	 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-2  + 1000^-3"		)")"
	assertEquals "1.5u" 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-2+512*1024^-3"	)")"
	assertEquals "500u" 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	500*1024^-2"			)")"
	assertEquals "1n" 		"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-3"				)")"
	assertEquals "500n" 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	500*1024^-3"			)")"
	assertEquals "1p" 		"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-4"				)")"
	assertEquals "500p" 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	500*1024^-4"			)")"
	assertEquals "1f" 		"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-5"	 			)")"
	assertEquals "500f"		"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	500*1024^-5"			)")"
	assertEquals "1n"		"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-5+1024^-3"	 	)")"
	assertEquals "1m"		"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	1024^-1 + 1024^-2"		)")"
	assertEquals "6.33q" 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	5*1000^-10")")"
	assertEquals "11mq" 	"$(koca_b2gmk "$( $KOCA_GNU_BC<<<"scale=64;	11*10^-3*10^-30")" 1000	)"
}
testB2Kbin() {
	# Asserting that if it was good with "base", this should be good too
	assertEquals "1k"	"$(koca_b2gmk 1024 1024)"
}
testB2Kdec() {
	assertEquals "1k" 	"$(koca_b2gmk 1000 1000)"
	assertEquals "1m" 	"$(koca_b2gmk $( $KOCA_GNU_BC<<<"scale=64;1/1000") 1000)"
}
testB2Kfun() {
	assertEquals "1n"	"$(koca_b2gmk $( $KOCA_GNU_BC<<<"scale=64;1/1000") 10)"
}
testNeg() {
	assertEquals "-1m" 	"$(koca_b2gmk $( $KOCA_GNU_BC<<<"scale=64;	-1024^-1"					))"
}
testFloat() {
	assertEquals "1k"	 "$(koca_b2gmk "1000.5"	"1000")"
	assertEquals "1.5"	 "$(koca_b2gmk "1.5" 	"1000")"
}
testFalse() {
	assertEquals '?' "$(koca_b2gmk 'N/A')"
}
source $(cd $(dirname "$0") ; pwd)/footer.sh
