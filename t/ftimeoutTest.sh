#!/usr/bin/env bash
source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
#source $here/../libs/cleanOnExit.sh
# assert <message> <valeur voulue> <valeur retournée>
export lock="/tmp/$$"
TMPDIR=${TMPDIR:=/tmp}
testFtimeout() {
	function f() 		{ echo stdout ; echo "stderr" >&2 ; sleep $1 ; }
	function f_exists()	{ ps -o pid,cmd | grep -qE '[0-9] sleep' ; }
	# ============
	koca_ftimeout 1 f 10 >/dev/null	2>/dev/null	; assertFalse "f wasn't killed" "f_exists"
	koca_ftimeout 2 f 1  >/dev/null	2>/dev/null	; assertFalse "f didn't exit" "f_exists"
	r="$(koca_ftimeout 2 f 1 2>/dev/null)"		; assertEquals "stdout of f wasn't catch" "stdout" "$r"
	r="$(koca_ftimeout 2 f 1 2>&1 >/dev/null)"	; assertEquals "stdout of f wasn't catch" "stderr" "$r"
	#s=$SECONDS; koca_ftimeout 1 sleep 10 ; e=$SECONDS ; assertEquals "Has not been killed" 1 "$((e-s))"
	#s=$SECONDS; koca_ftimeout 10 "sleep 1 ; false" ; ec=$? ; e=$SECONDS ; assertEquals "Didn't end" 1 "$((e-s))" ; assertEquals "Catch exit code" 1 "$ec"
}
source "$(cd "$(dirname "$0")" ; pwd)"/footer.sh
