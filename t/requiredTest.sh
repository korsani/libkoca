#!/usr/bin/env bash
source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
t="$(mktemp)"
type -a koca_required | sed -e 1d -e 's/__version__/1.1.1/g' > "$t"
source "$t"
testBase() {
	koca_required "1.1.0"
	assertEquals "function" "$(type -t koca_required)"
	koca_required "1.1.1"
	assertEquals "function" "$(type -t koca_required)"
	koca_required "1.1.1a" 2>/dev/null
	assertNull "$(type -t koca_required)"
	koca_required "1.1.2" 2>/dev/null
	assertNull "$(type -t koca_required)"
}
rm -f "$t"
source $(cd $(dirname "$0") ; pwd)/footer.sh
