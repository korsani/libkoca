#!/usr/bin/env bash
# $Id: zzfooter.sh 1128 2012-08-31 15:44:45Z gab $
#source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
testSetU() {
	function _outdated { echo $@ > /dev/null ; }
	set -u
	here="$(cd $(dirname "$0") ; pwd)"
	bn="$(basename "$0" Test.sh)"
	source "$here/../libs/$bn.sh" 2>&1 | grep -q "variable" ; re=$?
	assertFalse "Crash under set -u" "$re"
}
source $(cd $(dirname "$0") ; pwd)/footer.sh
