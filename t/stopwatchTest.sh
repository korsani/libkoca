#!/usr/bin/env bash
# shellcheck disable=SC1091,SC2164
source "$(cd "$(dirname "$0")" ; pwd)/bootstrap.sh"
#source $here/../libs/cleanOnExit.sh
# assert <message> <valeur voulue> <valeur retournée>
TMPDIR=${TMPDIR:=/tmp}
testSW() {
	koca_stopwatch stop ; r=$?
	assertEquals "Stop without start" "$r" "2"
	LC_ALL=C
	koca_stopwatch start ; sleep 1 ; printf -v d "%.0f" "$(koca_stopwatch stop)"
	assertEquals "Stopwatch did not take 1s" "1" "$d"
	koca_stopwatch flibidu ; r=$?
	assertEquals "Illegal command" "$r" "4"
	koca_stopwatch start
	koca_stopwatch reset ; r=$?
	assertEquals "Reset is not functionning" "0" "$r"
	d=( $(koca_stopwatch stop) )
	assertEquals "Did not reset" "1" "${#d[@]}"
	koca_stopwatch start
	koca_stopwatch delete ; r=$?
	assertEquals "Delete is not functionning" "0" "$r"
	d=( $(koca_stopwatch stop) )
	assertEquals "Did not delete" "0" "${#d[@]}"
	koca_stopwatch start
	koca_stopwatch get >/dev/null ; r=$?
	assertEquals "Get is not functionning" "0" "$r"
	assertNotNull "Get did not return value" "$(koca_stopwatch get)"
	koca_stopwatch delete
	koca_stopwatch start
	koca_stopwatch list >/dev/null ; r=$?
	assertEquals "List is not functionning" "0" "$r"
	assertEquals "Default timer not listed" "default" "$(koca_stopwatch list)"
	koca_stopwatch start a
	assertEquals "Timer not listed" "a default" "$(koca_stopwatch list | xargs)"
	koca_stopwatch delete

}
testSWNested() {
	koca_stopwatch start sw1
	koca_stopwatch start sw2
	sleep 1
	LC_ALL=C printf -v sw2 "%.0f" "$(koca_stopwatch stop sw2)"
	assertEquals "Stopwatch sw2 did not take 1s" 1 "$sw2"
	sleep 1
	LC_ALL=C printf -v sw1 "%.0f" "$(koca_stopwatch stop sw1)"
	assertEquals "Stopwatch sw1 did not take 2s" 2 "$sw1"
	koca_stopwatch start "a=b" 2>/dev/null ; r=$?
	assertFalse "Stopwatch timer name accept forbiden chars" "$r"
	koca_stopwatch start sw1 sw2
	s=( $(koca_stopwatch stop sw1 sw2) )
	assertEquals "Did not stop the 2 stopwatch" 2 "${#s[@]}"
	koca_stopwatch start sw1 sw2
	s=( $(koca_stopwatch stop sw1) )
	assertEquals "Did not stop 1 stopwatch on 2" "1" "${#s[@]}"
	koca_stopwatch stop sw2 >/dev/null
}
source "$(cd "$(dirname "$0")" ; pwd)/footer.sh"
