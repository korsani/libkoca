#!/usr/bin/env bash
# $Id: koca_getColor.sh 1164 2013-01-08 10:15:48Z gab $
source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
tput_af="setaf"
tput_ab="setab"
bold=bold
reset=sgr0
if [ "$(uname)" = "FreeBSD" ]
then
	tput_af=AF
	tput_ab=AB
	bold=md
	reset=me
fi
testGetColorReturn1onBadNumberOfArgument() {
	koca_getColor plop 2>/dev/null ; r=$?
	assertEquals "koca_getColor should return 1" "1" "$r"
}
testGetColorReturnACode() {
	unset a
	allcolors=$(koca_getColor list| grep '#' | cut -d ' ' -f 2)
	local r=$(tput sgr0)
	for c in $allcolors
	do
		koca_getColor a $c
		echo -n "$code$a$c$r "
		assertNotEquals "GetColor $c returned an empty string" "" "$a"
	done
	echo $r
}
testGetColorReturnAugmentedVar() {
	a=a
	koca_getColor a+ red
	b=$"$(tput $tput_af 1)"
	assertEquals "GetColor failed to return previous variable" "a$b" "$a"
}
testGetColorReturnClearedVar() {
	a=a
	koca_getColor a red
	b=$"$(tput $tput_af 1)"
	assertEquals "GetColor failed to return clean variable" "$b" "$a"
}
testGetColorReturnAugmentedVarDoubleColored() {
	a=a
	koca_getColor a+ red reset
	b=$"$(tput $tput_af 1)"$(tput $reset)
	assertEquals "GetColor failed to return previous variable double colored" "a$b" "$a"
}
testGetColorReturnCleanVarDoubleColored() {
	a=a
	koca_getColor a red reset
	b=$"$(tput $tput_af 1)"$(tput $reset)
	assertEquals "GetColor failed to return a clean variable when double colored " "$b" "$a"
}
testGetColorWorksEvenIfItsC() {
	koca_getColor c red
	b=$"$(tput $tput_af 1)"
	assertEquals "Bad code returned" "$b" "$c"
}
source $(cd $(dirname "$0") ; pwd)/footer.sh
