#!/usr/bin/env bash
source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
testload() {
	koca_load a 2>/dev/null ; r=$?
	assertEquals "Load accepts non int/float" "2" "$r"
	koca_load 10 ;r=$?
	assertTrue "Load is less than 10" "$r"
	koca_load -1 ;r=$?
	assertFalse "Load is less than 0" "$r"
	overload=10
	koca_load $overload ; r=$?
	assertTrue "I should be overloaded" "$r"
	koca_load 99.99 ; r=$?
	assertTrue "Load should accept .xx form" "$r"
	koca_load ; r=$?
	assertFalse "Load should return false" "$r"
}
source $(cd $(dirname "$0") ; pwd)/footer.sh
