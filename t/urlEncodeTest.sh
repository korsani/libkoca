#!/usr/bin/env bash
source $(cd $(dirname "$0") ; pwd)/bootstrap.sh
testUrlEncode()  {
	# https://www.urlencoder.io/
	assertEquals "de%20l%27%C3%A2me%21%20%28en%20toupie%20%3F%29%5B1%5D%2045%25" "$(koca_urlEncode "de l'âme! (en toupie ?)[1] 45%")"
	assertEquals "de%20l%27%C3%A2me%21%20%28en%20toupie%20%3F%29%5B1%5D%2045%25" "$(echo "plop" | koca_urlEncode "de l'âme! (en toupie ?)[1] 45%")"
	assertFalse "not false" "[ $(koca_urlEncode) ]"
	assertEquals "de%20l%27%C3%A2me%21%20%28en%20toupie%20%3F%29%5B1%5D%2045%25" "$(koca_urlEncode <<< "de l'âme! (en toupie ?)[1] 45%")"
	assertEquals "PR%C3%89VERT" "$(koca_urlEncode <<< "PRÉVERT")"
}
source $(cd $(dirname "$0") ; pwd)/footer.sh
